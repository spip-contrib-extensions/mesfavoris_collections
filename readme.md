# constantes

Quand on retire un favori d'une collection ou qu'on supprime une collection,
on peut choisir de conserver les favoris qui se retrouveraient orphelins (dans aucune collection)
plutôt que de les supprimer.

    define('_MESFAVORIS_COLLECTIONS_CONSERVER_ORPHELINS', true);

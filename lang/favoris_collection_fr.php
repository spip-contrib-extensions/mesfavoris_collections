<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_a' => 'Ajouter à une collection',
	'ajouter_autre' => 'Ajouter à une autre collection',
	'ajouter_a_long' => 'Ajouter ce contenu à une collection de favoris',
	'ajouter_lien_favoris_collection' => 'Ajouter cette collection de favoris',
	
	// C
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	
	// I
	'icone_creer_favoris_collection' => 'Créer une collection de favoris',
	'icone_modifier_favoris_collection' => 'Modifier cette collection de favoris',
	'info_1_favoris_collection' => 'Un itinéraire',
	'info_aucun_favoris_collection' => 'Aucune collection de favoris',
	'info_favoris_collections_auteur' => 'Les collections de favoris de cet auteur',
	'info_nb_favoris_collections' => '@nb@ collections de favoris',
	
	// R
	'ranger_id_favoris_collection_label' => 'Ajouter à une collection existante',
	'retirer_id_favoris_collection_label' => 'Retirer de cette collection',
	'ranger_titre_label' => 'Ajouter dans une nouvelle collection',
	'retirer_lien_favoris_collection' => 'Retirer cette collection de favoris',
	'retirer_tous_liens_favoris_collections' => 'Retirer toutes les collections de favoris',

	// T
	'texte_ajouter_favoris_collection' => 'Ajouter une collection de favoris',
	'texte_changer_statut_favoris_collection' => 'Cette collection de favoris est :',
	'texte_creer_associer_favoris_collection' => 'Créer et associer une collection de favoris',
	'titre_favoris_collection' => 'Collection de favoris',
	'titre_favoris_collections' => 'Collections de favoris',
	'titre_favoris_collections_rubrique' => 'Collections de favoris de la rubrique',
	'titre_langue_favoris_collection' => 'Langue de cette collection de favoris',
	'titre_logo_favoris_collection' => 'Logo de cette collection de favoris',
);

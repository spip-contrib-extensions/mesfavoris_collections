<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// I
	'mesfavoris_collections_description' => 'Ce plugin ajoute une notion de collections de favoris qui permet de ranger ses favoris dans des collections avec un titre et un URL, par exemple pour partager.',
	'mesfavoris_collections_nom' => 'Mes favoris : collections de favoris',
	'mesfavoris_collections_slogan' => 'Ranger ses favoris dans des collections',
);
